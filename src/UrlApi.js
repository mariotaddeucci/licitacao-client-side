var queryString = require('query-string');
var axios = require('axios');
var Cookies = require('js-cookie');

axios.interceptors.response.use(null, function(error) {

	if( error.response.status == 401){
		try{
			if(error.response.data.message.toUpperCase() == 'TOKEN REQUIRED'){
				location='/login';
				return;
			}
		}catch(e){

		}
	}
});

export default{
	domain:'http://127.0.0.1:5000/',
	api_prefix: 'api/',
	url : '',
	token(){
		return Cookies.get('token');	
	},
	setToken(t){
		Cookies.set('token',t);
	},
	authenticate(user,pass){
		return axios.post(this.domain+'login',{
			email:user,
			password:pass
		});
	},
	make(api, params){
		this.url = this.domain + this.api_prefix + api.trim().trim('/').trim() + '/'
		if((typeof params).toUpperCase() == 'OBJECT')
			this.url+= '?' + queryString.stringify(params);
		return this;
	},
	get(){	
		return axios({
			method:'get',
			url:this.url,
			headers:{
				'Authentication-Token':this.token()
			}
		});
	},
	post(data){
		return axios({
			method:'post',
			url:this.url,
			headers:{
				'Authentication-Token':this.token()
			},
			data:data
		});
	}

}