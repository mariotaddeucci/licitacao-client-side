import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './App.vue';
import CpontPagination from './components/Pagination.vue';
import CpontLoading from './components/Loading.vue';
import moment from 'moment';
import PageHome from './pages/Home.vue';
import PageLicitacoesVigentes from './pages/licitacao/Vigentes.vue';
import PageLogin from './pages/Login.vue';


Vue.filter('formatDate', function(value) {
	if (value) {
		return moment(String(value)).format('DD/MM/YYYY');
	}
});

Vue.filter('formatMoney', function(value) {
	if (value) {
		let val = (value/1).toFixed(2).replace('.', ',');
		return 'R$ ' + val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
	}
});

Vue.component('pagination', CpontPagination);
Vue.component('loading', CpontLoading);

Vue.use(VueRouter)

const routes =[
{path:'/', component:PageHome},
{path:'/licitacao/vigentes/',component:PageLicitacoesVigentes},
{path:'/login/',component:PageLogin, name:'Login'}
];



const router = new VueRouter({
	routes,
	mode:'history'
});


new Vue({
	el: '#app',
	router,
	render: h => h(App)
});